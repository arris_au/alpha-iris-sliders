<?php

namespace AlphaIris\Sliders\Console\Commands;

use Illuminate\Console\Command;
use TCG\Voyager\Traits\Seedable;

class AlphaIrisSlidersInstall extends Command
{
    use Seedable;

    protected $seedersPath = __DIR__.'/../../../database/seeds/';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'alpha-iris:sliders:install {--silent}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install and set up Alpha Iris Sliders';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->seed('AlphaIrisSliderMenuItemsSeeder');
        $this->seed('AlphaIrisSliderDataTypesSeeder');
        $this->seed('AlphaIrisSliderDataRowsSeeder');
        $this->seed('AlphaIrisSliderPermissionSeeder');
        $this->seed('AlphaIrisSliderPermissionRoleSeeder');

        return 0;
    }
}
