<?php

namespace AlphaIris\Sliders\Http\Controllers;

use AlphaIris\Core\Http\Controllers\AlphaIrisBreadController;
use AlphaIris\Sliders\Slider;
use AlphaIris\Sliders\SliderSlide;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Pvtl\VoyagerForms\Form;

class SliderSlideController extends AlphaIrisBreadController
{
    protected $slug = 'slides';

    public function create(Request $request, Slider $slider = null)
    {
        /** @var \Illuminate\View\View $result */
        $result = parent::create($request);

        return $result->with('slider', $slider);
    }

    public function edit(Request $request, $id, Slider $slider = null)
    {
        $result = parent::edit($request, $id);

        if (! $slider) {
            $slider = SliderSlide::find($id)->slider;
        }

        return $result->with('slider', $slider);
    }

    public function store(Request $request, Slider $slider = null)
    {
        if (is_null($slider)) {
            $slider = $request->get('slider');
        }

        if (! $slider instanceof \AlphaIris\Sliders\Slider) {
            $slider = Slider::find($slider);
        }

        $request->request->add(['slider_slides_slider_id' => $slider->id]);

        return parent::store($request);
    }

    public function update(Request $request, $id, $slider = null)
    {
        if (is_null($slider)) {
            $slider = $request->get('slider');
        }

        if (! $slider instanceof \AlphaIris\Sliders\Slider) {
            $slider = Slider::find($slider);
        }
        $request->request->add(['slider_slides_slider_id' => $slider->id]);

        return parent::update($request, $id);
    }
}
