<?php

namespace AlphaIris\Sliders\Http\BlockRenderers;

use AlphaIris\Core\Interfaces\LarabergRenderPipe;
use AlphaIris\Core\LarabergBlock;
use AlphaIris\Sliders\Slider;
use AlphaIris\Sliders\SliderSlide;
use Closure;

class SliderRenderer implements LarabergRenderPipe
{
    public function handle(LarabergBlock $block, Closure $next)
    {
        $sliderId = $block->prop('sliderId');
        $slider = ($sliderId && $sliderId !== -1) ? Slider::find($sliderId) : null;

        if (! $slider) {
            $block->setContent('Invalid slider');

            return $next($block);
        }

        try {
            $slide = $slider->slides()->first() ?: new SliderSlide();

            $sliderContent = view('alpha-iris-sliders::livewire.slide_preview', ['slide' => $slide])->render();
            $sliderContent = '<div style="'.$slider->wrapper_css."\">$sliderContent</div>";

            $block->setContent($sliderContent);
        } catch (\Exception $e) {
            $block->setContent($e->getMessage()."\n".$e->getTraceAsString());
        }

        return $next($block);
    }
}
