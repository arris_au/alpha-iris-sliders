<?php

namespace AlphaIris\Sliders\Http\Livewire;

use AlphaIris\Sliders\Slider;
use AlphaIris\Sliders\SliderSlide;
use Livewire\Component;

class AlphaIrisSlider extends Component
{
    protected $listeners = ['refreshSlide' => '$refresh'];
    public $sliderId;

    public function render()
    {
        $slider = Slider::find($this->sliderId);

        return view('alpha-iris-sliders::livewire/slider', [
            'slider' => $slider,
        ]);
    }
}
