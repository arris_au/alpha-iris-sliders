<?php

namespace AlphaIris\Sliders\Http\Livewire;

use AlphaIris\Sliders\SliderSlide;
use Livewire\Component;

class AlphaIrisSlidePreview extends Component
{
    protected $listeners = ['refreshSlide' => '$refresh'];
    public $title;
    public $subTitle;
    public $backgroundColour;
    public $image;
    public $textColour;
    public $textAlign;
    public $imagePosition;
    public $callToAction;

    public function render()
    {
        $dummySlide = new SliderSlide();
        $dummySlide->main_title = $this->title;
        $dummySlide->sub_title = $this->subTitle;
        $dummySlide->background_colour = $this->backgroundColour;
        $dummySlide->text_colour = $this->textColour;
        $dummySlide->image = empty($this->image) ? null : $this->image;
        $dummySlide->image_position = $this->imagePosition;
        $dummySlide->text_align = $this->textAlign;
        $dummySlide->call_to_action = $this->callToAction;

        return view('alpha-iris-sliders::livewire/slide_preview', [
            'slide' => $dummySlide,
        ]);
    }
}
