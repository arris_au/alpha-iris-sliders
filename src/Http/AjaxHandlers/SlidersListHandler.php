<?php

namespace AlphaIris\Sliders\Http\AjaxHandlers;

use AlphaIris\Sliders\Slider;
use Closure;
use Pvtl\VoyagerForms\Form;

class SlidersListHandler
{
    public function handle($requestResponse, Closure $next)
    {
        $request = $requestResponse[0];
        $response = $requestResponse[1];

        $mapped = Slider::all()->map(function ($item, $index) {
            return [
                'value' => $item['id'],
                'label' => $item['name'],
            ];
        })->toArray();

        array_unshift($mapped, [
            'value' => -1,
            'label' => 'Select slider...',
        ]);

        $response->setContent(json_encode($mapped));
        $response->header('Content-Type', 'application/json', true);

        return $next([$request, $response]);
    }
}
