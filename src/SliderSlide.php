<?php

namespace AlphaIris\Sliders;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Facades\Voyager;

class SliderSlide extends Model
{
    const IMAGE_POSITION_LEFT = 0;
    const IMAGE_POSITION_FULL = 1;
    const IMAGE_POSITION_RIGHT = 2;

    const IMAGE_COVER_COVER = 0;
    const IMAGE_COVER_CONTAIN = 1;

    const TEXT_ALIGN_LEFT = 0;
    const TEXT_ALIGN_CENTER = 1;
    const TEXT_ALIGN_RIGHT = 2;

    public $additional_attributes = ['slider_slides_slider_id'];

    public $fillable = [
        'slider_slides_slider_id',
        'main_title',
        'sub_title',
        'url',
        'image',
        'background_colour',
        'text_colour',
        'text_align',
        'image_position',
        'image_stretch',
        'call_to_action',
    ];

    public function getWrapperClassesAttribute()
    {
        $classes = [];
        // Text alignment
        switch ($this->text_align) {
            case self::TEXT_ALIGN_LEFT:
                $classes[] = 'text-left';
                break;
            case self::TEXT_ALIGN_CENTER:
                $classes[] = 'text-center';
                break;
            case self::TEXT_ALIGN_RIGHT:
                $classes[] = 'text-right';
                break;
        }

        return implode(' ', $classes);
    }

    public function getTitleClassesAttribute()
    {
        $classes = [];
        // Text alignment
        switch ($this->text_align) {
            case self::TEXT_ALIGN_LEFT:
                $classes[] = 'text-left';
                break;
            case self::TEXT_ALIGN_CENTER:
                $classes[] = 'text-center';
                break;
            case self::TEXT_ALIGN_RIGHT:
                $classes[] = 'text-right';
                break;
        }

        if ($this->image_position !== self::IMAGE_POSITION_FULL) {
            $classes[] = 'w-1/2';
            if (optional($this->slider)->navigation_arrows) {
                $classes[] = 'p-20';
            }
        } else {
            $classes[] = 'w-full';
            if (optional($this->slider)->navigation_arrows) {
                $classes[] = 'p-20';
            }
        }

        return implode(' ', $classes);
    }

    public function getSubtitleClassesAttribute()
    {
        $classes = [];
        // Text alignment
        switch ($this->text_align) {
            case self::TEXT_ALIGN_LEFT:
                $classes[] = 'text-left';
                break;
            case self::TEXT_ALIGN_CENTER:
                $classes[] = 'text-center';
                break;
            case self::TEXT_ALIGN_RIGHT:
                $classes[] = 'text-right';
                break;
        }

        return implode(' ', $classes);
    }

    public function getImageUrlAttribute()
    {
        return Voyager::image($this->image);
    }

    public function getWrapperCssAttribute()
    {
        $css = [];
        if ($this->image_position == self::IMAGE_POSITION_FULL && strlen($this->image) > 0) {
            $css[] = "background-image: url('".$this->image_url."')";

            switch ($this->image_stretch) {
                case self::IMAGE_COVER_COVER:
                    $css[] = 'background-size: cover';
                    break;
                case self::IMAGE_COVER_CONTAIN:
                    $css[] = 'background-size: contain';
                    break;
                }
        }

        if ($this->background_colour) {
            $css[] = 'background-color: '.$this->background_colour;
        }

        if ($this->text_colour) {
            $css[] = 'color: '.$this->text_colour;
        }

        return implode(';', $css);
    }

    public function slider()
    {
        return $this->belongsTo(Slider::class, 'slider_slides_slider_id');
    }

    public function getTitleAttribute()
    {
        return $this->main_title;
    }
}
