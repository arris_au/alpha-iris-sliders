<?php

namespace AlphaIris\Sliders\Providers;

use AlphaIris\Core\Services\AlphaIris;
use AlphaIris\Core\Services\LarabergBlockService;
use AlphaIris\Sliders\AlphaIrisSlidersBladeDirectives;
use AlphaIris\Sliders\Console\Commands\AlphaIrisSlidersInstall;
use AlphaIris\Sliders\Http\AjaxHandlers\SlidersListHandler;
use AlphaIris\Sliders\Http\BlockRenderers\SliderRenderer;
use AlphaIris\Sliders\Http\Livewire\AlphaIrisSlidePreview;
use AlphaIris\Sliders\Http\Livewire\AlphaIrisSlider;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Livewire\Livewire;

class SlidersServiceProvider extends ServiceProvider
{
    public function boot()
    {
        Livewire::component('alpha-iris-slide-preview', AlphaIrisSlidePreview::class);
        Livewire::component('alpha-iris-slider', AlphaIrisSlider::class);
        $path = asset('vendor/alpha-iris/alpha-iris-sliders-admin.js');
        AlphaIris::registerAdminScript('alpha-iris-sliders-admin', $path);
        AlphaIris::registerAdminStylesheet('alpha-iris-sliders-admin', asset('vendor/alpha-iris/alpha-iris-sliders-admin.css'));
        AlphaIris::registerScript('alpha-iris-sliders', asset('vendor/alpha-iris/alpha-iris-sliders.js'));
        AlphaIris::registerStylesheet('alpha-iris-sliders', asset('vendor/alpha-iris/alpha-iris-sliders.css'));
        AlphaIris::registerAjaxHandler('sliders-list', SlidersListHandler::class);
        LarabergBlockService::registerRenderer('alpha-iris/slider-block', SliderRenderer::class);

        $this->loadViewsFrom(__DIR__.'/../../resources/views/vendor/voyager', 'voyager');
        $this->loadViewsFrom(__DIR__.'/../../resources/views/vendor/alpha-iris/sliders', 'alpha-iris-sliders');

        $this->loadMigrationsFrom(__DIR__.'/../../database/migrations');

        $this->loadViewsFrom([
            'resources/views/',
            __DIR__.'/../../resources/views/vendor/alpha-iris',
        ], 'alpha-iris-sliders');

        $this->publishes([
            __DIR__.'/../../database/seeds/' => database_path('seeds'),
            __DIR__.'/../../publish/js/alpha-iris-sliders.js' => public_path('vendor/alpha-iris/alpha-iris-sliders.js'),
            __DIR__.'/../../publish/css/alpha-iris-sliders.css' => public_path('vendor/alpha-iris/alpha-iris-sliders.css'),
            __DIR__.'/../../publish/js/alpha-iris-sliders-admin.js' => public_path('vendor/alpha-iris/alpha-iris-sliders-admin.js'),
            __DIR__.'/../../publish/css/alpha-iris-sliders-admin.css' => public_path('vendor/alpha-iris/alpha-iris-sliders-admin.css'),
        ], 'ai-sliders');

        $this->loadRoutesFrom(__DIR__.'../../../routes/web.php');
        Blade::directive('alphaIrisSlider', [AlphaIrisSlidersBladeDirectives::class, 'alphaIrisSlider']);
    }

    public function register()
    {
        $this->commands([
            AlphaIrisSlidersInstall::class,
        ]);
    }
}
