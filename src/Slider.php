<?php

namespace AlphaIris\Sliders;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    const BULLET_POSITION_LEFT = 0;
    const BULLET_POSITION_CENTER = 1;
    const BULLET_POSITION_RIGHT = 2;

    public $fillable = [
        'navigation_bullets',
        'bullets_position',
        'navigation_arrows',
        'max_height',
    ];

    public function slides()
    {
        return $this->hasMany(SliderSlide::class, 'slider_slides_slider_id', 'id');
    }

    public function getWrapperCssAttribute()
    {
        $css = [];
        $css[] = 'height: '.($this->max_height ?: '600px');

        return implode('; ', $css);
    }

    public function getIntervalMsAttribute()
    {
        return 3000;
    }
}
