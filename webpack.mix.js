const mix = require('laravel-mix');

const tailwindcss = require('tailwindcss');

  // webpack.mix.js
  mix.scripts(["resources/assets/js/alpha-iris-sliders-admin.js"],
   "publish/js/alpha-iris-sliders-admin.js");

     // webpack.mix.js
  mix.scripts(["resources/assets/js/alpha-iris-sliders.js"],
  "publish/js/alpha-iris-sliders.js");

  mix.sass('resources/assets/sass/alpha-iris-sliders.scss', 'publish/css')
   .options({
      processCssUrls: false,
      postCss: [ tailwindcss('tailwind.config.js') ],
  });

  mix.sass('resources/assets/sass/alpha-iris-sliders-admin.scss', 'publish/css')
   .options({
      processCssUrls: false,
      postCss: [ tailwindcss('tailwind.config.js') ],
  });  