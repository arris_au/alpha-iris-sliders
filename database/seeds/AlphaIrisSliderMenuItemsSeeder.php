<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Menu;
use TCG\Voyager\Models\MenuItem;

class AlphaIrisSliderMenuItemsSeeder extends Seeder
{
    public function run()
    {
        $adminMenu = Menu::where('name', 'admin')->firstOrFail();

        $menuItem = MenuItem::firstOrNew([
                    'menu_id' => $adminMenu->id,
                    'title' => 'Sliders',
                    'url' => '',
                    'route' => 'voyager.sliders.index',
        ]);

        if (! $menuItem->exists) {
            $menuItem->fill([
                'target' => '_self',
                'icon_class' => 'voyager-tv',
                'color' => null,
                'parent_id' => null,
                'order' => 6,
            ])->save();
        }
    }
}
