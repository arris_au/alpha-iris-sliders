<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Permission;

class AlphaIrisSliderPermissionSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     */
    public function run()
    {
        Permission::generateFor('sliders');
        Permission::generateFor('slider_slides');
    }
}
