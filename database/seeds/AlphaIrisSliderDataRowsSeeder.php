<?php

use AlphaIris\Sliders\Slider;
use AlphaIris\Sliders\SliderSlide;
use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataRow;
use TCG\Voyager\Models\DataType;

class AlphaIrisSliderDataRowsSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     */
    public function run()
    {
        $sliderType = $this->addSliderBread();
        $sildeType = $this->addSlideBread();

        $this->addRelationship(
            $sliderType,
            $sildeType,
            'hasMany',
            'slider_slides_slider_id',
            'Slides',
            'alpha-iris-sliders::slider_edit_slides',
            true
        );

        $this->addRelationship(
            $sildeType,
            $sliderType,
            'belongsTo',
            'slider_slides_slider_id',
            'Slider'
        );
    }

    protected function addRelationship($fromType, $toType, $relType, $fkId, $title, $renderer = null, $visible = false)
    {
        $relationshipName = $fromType->name.'_'.strtolower($relType).'_'.$toType->name.'_relationship';
        $dataRow = $this->dataRow($fromType, $relationshipName);
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type'         => 'relationship',
                'display_name' => $title,
                'required'     => 0,
                'browse'       => $visible ? 1 : 0,
                'read'         => $visible ? 1 : 0,
                'edit'         => $visible ? 1 : 0,
                'add'          => $visible ? 1 : 0,
                'delete'       => $visible ? 1 : 0,
                'details'      => [
                    'model' => $toType->model_name,
                    'table' => $toType->name,
                    'type' => $relType,
                    'column' => $fkId,
                    'key' => 'id',
                    'label' => 'id',
                    'view' => $renderer,
                ],
                'order'        => $fromType->rows->last()->order + 1,
            ])->save();
        }
    }

    protected function addSliderBread()
    {
        $sliderDataType = DataType::where('slug', 'sliders')->firstOrFail();

        $dataRow = $this->dataRow($sliderDataType, 'id');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type'         => 'number',
                'display_name' => 'ID',
                'required'     => 1,
                'browse'       => 0,
                'read'         => 0,
                'edit'         => 0,
                'add'          => 0,
                'delete'       => 0,
                'details'      => '',
                'order'        => 1,
            ])->save();
        }

        $dataRow = $this->dataRow($sliderDataType, 'name');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type'         => 'text',
                'display_name' => 'Name',
                'required'     => 1,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 1,
                'details'      => '',
                'order'        => 2,
            ])->save();
        }

        $dataRow = $this->dataRow($sliderDataType, 'navigation_bullets');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type'         => 'checkbox',
                'display_name' => 'Navigation Bullets',
                'required'     => 0,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 1,
                'details'      => '',
                'order'        => 3,
            ])->save();
        }

        $dataRow = $this->dataRow($sliderDataType, 'bullets_position');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type'         => 'select_dropdown',
                'display_name' => 'Bullets Position',
                'required'     => 0,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 1,
                'details'      => [
                    'default' => Slider::BULLET_POSITION_CENTER,
                    'options' => [
                        Slider::BULLET_POSITION_CENTER => 'Center',
                        Slider::BULLET_POSITION_LEFT => 'Left',
                        Slider::BULLET_POSITION_RIGHT => 'Right',
                    ],
                ],
                'order'        => 4,
            ])->save();
        }

        $dataRow = $this->dataRow($sliderDataType, 'navigation_arrows');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type'         => 'checkbox',
                'display_name' => 'Navigation Arrows',
                'required'     => 0,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 1,
                'details'      => '',
                'order'        => 3,
            ])->save();
        }

        $dataRow = $this->dataRow($sliderDataType, 'max_height');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type'         => 'text',
                'display_name' => 'Max Height',
                'required'     => 1,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 1,
                'details'      => '',
                'order'        => 4,
            ])->save();
        }

        $dataRow = $this->dataRow($sliderDataType, 'interval');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type'         => 'number',
                'display_name' => 'Interval (ms)',
                'required'     => 1,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 1,
                'details'      => '',
                'order'        => 5,
            ])->save();
        }

        return $sliderDataType;
    }

    protected function addSlideBread()
    {
        $slideDataType = DataType::where('slug', 'slides')->firstOrFail();

        $dataRow = $this->dataRow($slideDataType, 'id');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type'         => 'number',
                'display_name' => 'ID',
                'required'     => 1,
                'browse'       => 0,
                'read'         => 0,
                'edit'         => 0,
                'add'          => 0,
                'delete'       => 0,
                'details'      => '',
                'order'        => 1,
            ])->save();
        }

        $dataRow = $this->dataRow($slideDataType, 'main_title');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type'         => 'text',
                'display_name' => 'Title',
                'required'     => 1,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 1,
                'details'      => '',
                'order'        => 3,
            ])->save();
        }

        $dataRow = $this->dataRow($slideDataType, 'sub_title');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type'         => 'text',
                'display_name' => 'Sub Title',
                'required'     => 0,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 1,
                'details'      => '',
                'order'        => 4,
            ])->save();
        }

        $dataRow = $this->dataRow($slideDataType, 'call_to_action');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type'         => 'text',
                'display_name' => 'Call To Action',
                'required'     => 0,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 1,
                'details'      => '',
                'order'        => 5,
            ])->save();
        }

        $dataRow = $this->dataRow($slideDataType, 'url');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type'         => 'text',
                'display_name' => 'URL',
                'required'     => 0,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 1,
                'details'      => '',
                'order'        => 6,
            ])->save();
        }

        $dataRow = $this->dataRow($slideDataType, 'image');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type'         => 'media_picker',
                'display_name' => 'Image',
                'required'     => 0,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 1,
                'details'      => '',
                'order'        => 7,
            ])->save();
        }

        $dataRow = $this->dataRow($slideDataType, 'text_align');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type'         => 'select_dropdown',
                'display_name' => 'Text Alignment',
                'required'     => 1,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 1,
                'details'      => [
                    'default' => SliderSlide::TEXT_ALIGN_LEFT,
                    'options' => [
                        SliderSlide::TEXT_ALIGN_LEFT  => 'Left',
                        SliderSlide::TEXT_ALIGN_CENTER => 'Center',
                        SliderSlide::TEXT_ALIGN_RIGHT => 'Right',
                    ],
                ],
                'order'        => 8,
            ])->save();
        }

        $dataRow = $this->dataRow($slideDataType, 'background_colour');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type'         => 'color',
                'display_name' => 'Background Colour',
                'required'     => 0,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 1,
                'details'      => '',
                'order'        => 9,
            ])->save();
        }

        $dataRow = $this->dataRow($slideDataType, 'text_colour');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type'         => 'color',
                'display_name' => 'Text Colour',
                'required'     => 0,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 1,
                'details'      => '',
                'order'        => 10,
            ])->save();
        }

        $dataRow = $this->dataRow($slideDataType, 'image_position');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type'         => 'select_dropdown',
                'display_name' => 'Image Position',
                'required'     => 1,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 1,
                'details'      => [
                    'default' => SliderSlide::IMAGE_POSITION_FULL,
                    'options' => [
                        SliderSlide::IMAGE_POSITION_FULL  => 'Full/Cover',
                        SliderSlide::IMAGE_POSITION_LEFT => 'Left',
                        SliderSlide::IMAGE_POSITION_RIGHT => 'Right',
                    ],
                ],
                'order'        => 11,
            ])->save();
        }

        $dataRow = $this->dataRow($slideDataType, 'image_stretch');
        if (! $dataRow->exists) {
            $dataRow->fill([
                'type'         => 'select_dropdown',
                'display_name' => 'Image Stretch',
                'required'     => 1,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 1,
                'details'      => [
                    'default' => SliderSlide::IMAGE_COVER_COVER,
                    'options' => [
                        SliderSlide::IMAGE_COVER_COVER  => 'Full/Cover',
                        SliderSlide::IMAGE_COVER_CONTAIN => 'Contain',
                    ],
                ],
                'order'        => 12,
            ])->save();
        }

        return $slideDataType;
    }

    /**
     * [dataRow description].
     *
     * @param [type] $type  [description]
     * @param [type] $field [description]
     *
     * @return [type] [description]
     */
    protected function dataRow($type, $field)
    {
        return DataRow::firstOrNew([
           'data_type_id' => $type->id,
           'field'        => $field,
       ]);
    }
}
