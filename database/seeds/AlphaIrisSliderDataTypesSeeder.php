<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataType;

class AlphaIrisSliderDataTypesSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     */
    public function run()
    {
        $dataType = $this->dataType('slug', 'sliders');
        if (! $dataType->exists) {
            $dataType->fill([
                'name'                  => 'sliders',
                'display_name_singular' => 'Slider',
                'display_name_plural'   => 'Sliders',
                'icon'                  => 'voyager-file-text',
                'model_name'            => 'AlphaIris\\Sliders\\Slider',
                'controller'            => '\\AlphaIris\\Sliders\\Http\\Controllers\\SliderController',
                'generate_permissions'  => 1,
                'description'           => '',
            ])->save();
        }

        if ($dataType->exists) {
            $dataType->update([
                'model_name' => 'AlphaIris\\Sliders\\Slider',
                'controller' => '\\AlphaIris\\Sliders\\Http\\Controllers\\SliderController',
            ]);
        }

        $dataType = $this->dataType('slug', 'slides');
        if (! $dataType->exists) {
            $dataType->fill([
                'name'                  => 'slider_slides',
                'display_name_singular' => 'Slide',
                'display_name_plural'   => 'Slides',
                'icon'                  => 'voyager-file-text',
                'model_name'            => 'AlphaIris\\Sliders\\SliderSlide',
                'controller'            => '\\AlphaIris\\Sliders\\Http\\Controllers\\SliderSlideController',
                'generate_permissions'  => 1,
                'description'           => '',
            ])->save();
        }

        if ($dataType->exists) {
            $dataType->update([
                'model_name' => 'AlphaIris\\Sliders\\SliderSlide',
                'controller' => '\\AlphaIris\\Sliders\\Http\\Controllers\\SliderSlideController',
            ]);
        }
    }

    /**
     * [dataType description].
     *
     * @param [type] $field [description]
     * @param [type] $for   [description]
     *
     * @return [type] [description]
     */
    protected function dataType($field, $for)
    {
        return DataType::firstOrNew([$field => $for]);
    }
}
