<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSlidersTables extends Migration
{
    public function up()
    {
        Schema::create('sliders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->boolean('navigation_bullets')->default(false);
            $table->integer('bullets_position')->nullable();
            $table->integer('interval')->default(3000);
            $table->boolean('navigation_arrows')->default(false);
            $table->string('max_height')->nullable();
            $table->timestamps();
        });

        Schema::create('slider_slides', function (Blueprint $table) {
            $table->increments('id');
            $table->foreignId('slider_slides_slider_id');
            $table->string('main_title');
            $table->string('sub_title')->nullable();
            $table->string('call_to_action')->nullable();
            $table->string('url')->nullable();
            $table->string('image')->nullable();
            $table->string('background_colour')->nullable();
            $table->string('text_colour')->nullable();
            $table->integer('text_align')->nullable();
            $table->integer('image_position')->nullable();
            $table->integer('image_stretch')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('slider_slides');
        Schema::drop('sliders');
    }
}
