document.addEventListener('laraberg:init', function(e){
    window.LarabergAI.fullWidthBlocks.push('alpha-iris/slider-block');
    var el = window.wp.element.createElement;
    
    const myBlock =  {
      title: 'Slider',
      icon: 'universal-access-alt',
      category: 'layout',
      attributes: {
        sliderId: {
          type: 'string',
        },
        className: {
          type: 'string',
          default: ''
        },
      },
      edit:function(props) {
        let response = '';
        $.ajax({
          async: false,
          url: Laraberg.serverRenderUrl,
          type: 'POST',
          data: {
            blockType: 'alpha-iris/slider-block',
            props: props.attributes
          },
          success: function(result){
            response = result;
          }
        });
        updateSlider = function (event) {
          props.setAttributes({sliderId: event.target.value})
        };
  
        let options = [];
        $.ajax({
          url: AlphaIris.ajaxUrl,
          async: false,
          type: 'POST',
          data: {
            method: 'sliders-list'
          },
          success: function(result){
            options = result;
          }
        });
  
        let select = el(
          wp.components.SelectControl,
          {
            value: props.attributes.sliderId, 
            label: "Select Slider",
            options: options,
            value: props.attributes.sliderId,
            onChange: function(value){
              props.setAttributes({sliderId: value});
            }
          }
        );
  
        const controls = [
          el(
            wp.editor.InspectorControls,
            {},
            el(wp.components.PanelBody, {},
            el(wp.components.PanelRow, {}, 
              select,
              //React.createElement("input", { type: "text", value: props.attributes.sliderId, onChange: updateForm })
              )
            ),
          ),
        ];      
  
        return [controls, el(
          'div',
          {className: props.attributes.className +' wp-block-alpha-iris-slider-block', dangerouslySetInnerHTML: {__html: response}}
        )];
  
        return result;
      },
    
      save:function(props){
        return el('div', {
          dangerouslySetInnerHTML: { __html: '<livewire:alpha-iris-slider slider-id="' + props.attributes.sliderId +'"/>'}
        });
      }
    }
    
    Laraberg.registerBlock('alpha-iris/slider-block', myBlock)
  });
  
  