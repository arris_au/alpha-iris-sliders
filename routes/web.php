<?php
Route::group(['prefix' => 'admin', 'middleware' => 'admin.user'], function(){
	Route::resource('sliders/{slider}/slides', \AlphaIris\Sliders\Http\Controllers\SliderSlideController::class);
});

