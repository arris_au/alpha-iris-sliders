AISlider = function(sliderWrapper){
    this.slides = [];
    let me = this;  

    this.sliderWrapper = sliderWrapper;

    let items = this.sliderWrapper.querySelectorAll('.alpha-iris-slide');
    for(var i=0; i < items.length;i++){
        this.slides.push(items[i].getAttribute('data-slide-id'));
    }
    this.currentSlide = this.slides.indexOf(this.sliderWrapper.querySelectorAll('.alpha-iris-active-slide')[0].getAttribute('data-slide-id'));

    if(this.sliderWrapper.querySelectorAll('.prev').length){
        this.sliderWrapper.querySelectorAll('.prev')[0].addEventListener('click', function(){
            me.prev();
        });    
    }

    if(this.sliderWrapper.querySelectorAll('.next').length){
        this.sliderWrapper.querySelectorAll('.next')[0].addEventListener('click', function(){
            me.next();
        });       
    }

    this.autoAdvance = function(){
        var me = this;
        this.next();
        this.timeout = window.setTimeout(function(){
            me.autoAdvance();
        }, this.interval);
    }


    this.interval = sliderWrapper.querySelectorAll('.alpha-iris-slider')[0].getAttribute('data-interval');
    if(this.slides.length > 1){       
        this.timeout = window.setTimeout(function(){
            me.autoAdvance();
        }, this.interval);
    }


    this.next = function(){
        window.clearTimeout(this.timeout);
        this.currentSlide++;        
        if(this.currentSlide == this.slides.length){
            this.currentSlide = 0;
        };
        this.displaySlide(this.slides[this.currentSlide]);
    }

    this.prev = function(){
        this.currentSlide--;
        if(this.currentSlide == -1){
            this.currentSlide = this.slides.length-1;
        };
        this.displaySlide(this.slides[this.currentSlide]);
    }    

    function removeClasses(parent, className){
        items = parent.querySelectorAll('.' + className);
        for(var i =0; i < items.length; i++){
            items[i].classList.remove(className);
        }
    }

    this.displaySlide = function(slideId){
        removeClasses(this.sliderWrapper, 'alpha-iris-active-slide');
        removeClasses(this.sliderWrapper, 'alpha-iris-slider-bullet-active');

        document.getElementById('slide-' + slideId).classList.add('alpha-iris-active-slide');
        if(document.getElementById('bullet-' + slideId)){
            document.getElementById('bullet-' + slideId).classList.add('alpha-iris-slider-bullet-active');
        }
    }
}

AISliders = new function(){
    this.sliders = [];
    var me = this;

    document.addEventListener('DOMContentLoaded', function(){
        sliders = document.getElementsByClassName('alpha-iris-slider');
    
        for(var i=0; i < sliders.length; i++){
            slider = sliders[i];
            me.sliders.push(new AISlider(slider.parentElement));
        }
    });

}();


function nextSlide(){
    let activeSlide = document.querySelector('.slide.translate-x-0');
    activeSlide.classList.remove('translate-x-0');
    activeSlide.classList.add('-translate-x-full');
    
    let nextSlide = activeSlide.nextElementSibling;
    if(!nextSlide.classList.contains('slide')){
        nextSlide = activeSlide.parentElement.firstElementChild;
    }
    nextSlide.classList.remove('translate-x-full');
    nextSlide.classList.add('translate-x-0');
}

function previousSlide(){
    let activeSlide = document.querySelector('.slide.translate-x-0');
    activeSlide.classList.remove('translate-x-0');
    activeSlide.classList.add('-translate-x-full');
    
    let nextSlide = activeSlide.nextElementSibling;
    if(!nextSlide.classList.contains('slide')){
        nextSlide = activeSlide.parentElement.firstElementChild;
    }
    nextSlide.classList.remove('translate-x-full');
    nextSlide.classList.add('translate-x-0');
}
