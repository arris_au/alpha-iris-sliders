@switch($view)
    @case('browse')
        {{ $data->slides->count() }}
        @break
    @case('read')
        @break
    @case('edit')
        <div class="flex border-0 h-56">
            @foreach($dataTypeContent->slides as $slide)
                <a href="{{ route('voyager.slides.edit', $slide)}}">
                    <livewire:alpha-iris-slide-preview 
                    title="{{$slide->main_title}}" 
                    subTitle="{{$slide->sub_title}}" 
                    backgroundColour="{{$slide->background_colour}}" 
                    image="{{$slide->image}}" 
                    textColour="{{$slide->text_colour}}" 
                    textAlign="{{$slide->text_align}}" 
                    imagePosition="{{$slide->image_position}}" 
                    callToAction="{{$slide->call_to_action}}"
                    />                
                </a>
            @endforeach
            <a href="{{ route('slides.create', $dataTypeContent) }}">
                <livewire:alpha-iris-slide-preview 
                    title="New Slide"
                    textAlign="1"
                    image=""
                />
            </a>
        </div>
        @break
    @case('add')
        @break
    @case('order')
        @break
@endswitch