<div class="alpha-iris-slider">
        <div class="p-8 inset-0 h-full w-full flex items-center text-xl border-0 w-80 {!! optional($slide)->wrapper_classes !!}" style="{!! optional($slide)->wrapper_css !!}">
                @if($slide->image && ($slide->image_position == \AlphaIris\Sliders\SliderSlide::IMAGE_POSITION_LEFT || $slide->image_position == \AlphaIris\Sliders\SliderSlide::IMAGE_POSITION_RIGHT ))
                <div class="slide-image-position-{{optional($slide)->image_position}}">
                        <img src="{{$slide->image_url}}" alt="{{$slide->title}}"  border=0/>
                </div>
                @endif
                <div class="w-full {{optional($slide)->title_classes}}">
                        <div class="text-4xl font-bold slide-title">{{ $slide->title }}</div>
                        <div class="text-2xl slide-subtitle">{{ $slide->sub_title }}</div>
                        @if($slide->call_to_action)
                                <div class="slide-cta">
                                        <a href="#" >{{$slide->call_to_action}}</a>
                                </div>
                        @endif                
                </div>
        </div>
</div>