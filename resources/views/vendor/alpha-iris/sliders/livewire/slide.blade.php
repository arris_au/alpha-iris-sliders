<div class="alpha-iris-slide absolute opacity-0 h-full p-4 flex items-center {{$visible ? 'alpha-iris-active-slide' : ''}} {!! $slide->wrapper_classes!!} " id="slide-{{$slide->id}}" style="{!! $slide->wrapper_css !!}" data-slide-id="{{$slide->id}}">
        @if($slide->image_position == \AlphaIris\Sliders\SliderSlide::IMAGE_POSITION_LEFT || $slide->image_position == \AlphaIris\Sliders\SliderSlide::IMAGE_POSITION_RIGHT )
        <div class="slide-image slide-image-position-{{$slide->image_position}} h-full w-1/2 p-4">
                <div style="background-image: url('{{$slide->image_url}}')" class="h-full">
                &nbsp;
                </div>
        </div>
        @endif
        <div class="{{$slide->title_classes}}">
                <div class="md:text-8xl text-2xl font-bold slide-title font-bold slide-title">{{ $slide->title }}</div>
                <div class="md:text-6xl text-2xl slide-subtitle">{{ $slide->sub_title }}</div>
                @if($slide->call_to_action)
                        <div class="slide-cta">
                                <a href="{{$slide->url}}" >{{$slide->call_to_action}}</a>
                        </div>
                @endif
        </div>
</div>