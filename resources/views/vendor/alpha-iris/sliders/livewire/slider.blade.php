<div class="relative alpha-iris-slider carousel relative" style="{!!optional($slider)->wrapper_css !!}" data-interval="{{optional($slider)->interval_ms}}">
@if($slider)
	<div class="carousel-inner relative overflow-hidden w-full h-full">
        @foreach($slider->slides as $slide)
            @include('alpha-iris-sliders::livewire/slide',['slide' => $slide, 'visible' => $loop->first])
        @endforeach

        @if($slider->navigation_arrows)
        <label class="prev left-0  ml-2 md:ml-10 ">‹</label>
        <label class="next right-0 mr-2 md:mr-10 ">›</label>
        @endif

		<!-- Add additional indicators for each slide-->
        @if($slider->navigation_bullets)
		<ol class="alpha-iris-slider-bullets slider-bullets-{{ $slider->bullets_position }}">
            @foreach($slider->slides as $slide)
                <li class="inline-block mr-3">
                    <label id="bullet-{{$slide->id}}" class="carousel-bullet {{ $loop->first? 'alpha-iris-slider-bullet-active' : ''}} cursor-pointer block text-4xl text-white hover:text-blue-700">•</label>
                </li>
            @endforeach
		</ol>
        @endif
		
	</div>
@else
    Invalid slider 
@endif    
</div>